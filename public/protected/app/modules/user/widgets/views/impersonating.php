<?php
/**
 * Impersonating view file for the NImpersonating widget
 *
 * @author Newicon, Steven O'Brien <steven.obrien@newicon.net>
 * @link http://github.com/newicon/Nii
 * @copyright Copyright &copy; 2009-2011 Newicon Ltd
 * @license http://newicon.net/framework/license/
 */
?>

<?php if($impersonator = Yii::app()->getModule('user')->ImpersonatingUser) : ?>
	<div class="alert info">
		<p>
			Currently impersonating <?php echo Yii::app()->user->name ?> (UID:<?php echo Yii::app()->user->record->id() ?><?php echo Yii::app()->user->record->roleDescription ? ', Role:'.Yii::app()->user->record->roleDescription : '' ?>) 
			<a class="pull-right" href="<?php echo NHtml::url('/user/admin/restore') ?>">Restore permissions to <?php echo $impersonator->name ?></a>
		</p>
	</div>
<?php endif; ?>