<?php

class NTime
{

	/**
	 * Returns a nicely formatted date string for given Datetime string.
	 *
	 * @param string $dateString Datetime string
	 * @param int $format Format of returned date
	 * @return string Formatted date string
	 */
	public static function nice($dateString = null, $format = 'D, M jS Y, H:i')
	{
		return date($format, self::getUnix($dateString));
	}

	/**
	 * Takes a string or timestamp and returns a unix timestamp
	 * if $dateVarient is empty return the current time
	 * @param varient $dateVarient
	 * @return int unix timestamp
	 */
	public static function getUnix($dateVarient = null)
	{
		if ($dateVarient === null)
			return time();
		return is_string($dateVarient) ? strtotime($dateVarient) : $dateVarient;
	}

	/**
	 * Returns a formatted descriptive date string for given datetime string.
	 *
	 * If the given date is today, the returned string could be "Today, 6:54 pm".
	 * If the given date was yesterday, the returned string could be "Yesterday, 6:54 pm".
	 * If $dateString's year is the current year, the returned string does not
	 * include mention of the year.
	 *
	 * @param string $dateString Datetime string or Unix timestamp
	 * @param bool $removeTime whether to show or remove the time
	 * @return string Described, relative date string
	 */
	public static function niceShort($dateString = null, $removeTime = false)
	{

		$date = self::getUnix($dateString);

		$y = (self::isThisYear($date)) ? '' : ' Y';

		if (self::isToday($date)) {
			$ret = ($removeTime) ? 'Today' : sprintf('Today, %s', date("g:i a", $date));
		} elseif (self::wasYesterday($date)) {
			$ret = ($removeTime) ? 'Yesterday' : sprintf('Yesterday, %s', date("g:i a", $date));
		} else {
			$ret = date("M jS{$y}", $date);
		}

		return $ret;
	}

	/**
	 * Returns a formatted descriptive date string for given datetime string.
	 * 
	 * If the given date is today the returned string will be like: "2:34 pm"
	 * If the given date is yesterday then the returned string will just state: "Yesterday"
	 * If the given date is laster than yesterday it will just show a date like: 13/02/2010
	 * 
	 * @param string $dateString
	 * @return string 
	 */
	public static function niceShorter($dateString = null, $class = 'faded')
	{
		$date = self::getUnix($dateString);
		$y = (self::isThisYear($date)) ? '' : ' Y';
		if (self::isToday($date)) {
			$ret = sprintf('<span class="time">%s</span> <span class="' . $class . '">%s</span>', date("g:i", $date), date("A", $date));
		} elseif (self::wasYesterday($date)) {
			$ret = '<span class="' . $class . '">Yesterday</span>';
		} else {
			$ret = '<span class="' . $class . '">' . date("d/m/y", $date) . '</span>';
		}
		return $ret;
	}

	/**
	 * Returns true if given date is today.
	 *
	 * @param int $date Unix timestamp
	 * @return boolean True if date is today
	 */
	public static function isToday($date)
	{
		return date('Y-m-d', $date) == date('Y-m-d', time());
	}

	/**
	 * Returns true if given date is a weekday (mon - fri).
	 *
	 * @param int $date Unix timestamp
	 * @return boolean True if date is a weekday
	 */
	public static function isWeekDay($date)
	{
		$w = date('w', $date);
		return $w != 0 || $w != 6;
	}

	/**
	 * Returns true if given date is a weekend (saturday / sunday).
	 *
	 * @param int $date Unix timestamp
	 * @return boolean True if date is a weekend
	 */
	public static function isWeekEnd($date)
	{
		$w = date('w', $date);
		return $w == 0 || $w == 6;
	}

	/**
	 * Returns true if given date was yesterday
	 *
	 * @param string $date Unix timestamp
	 * @return boolean True if date was yesterday
	 */
	public static function wasYesterday($date)
	{
		return date('Y-m-d', $date) == date('Y-m-d', strtotime('yesterday'));
	}

	/**
	 * Returns true if given date is in this year
	 *
	 * @param string $date Unix timestamp
	 * @return boolean True if date is in this year
	 */
	public static function isThisYear($date)
	{
		return date('Y', $date) == date('Y', time());
	}

	/**
	 * Returns true if given date is in this week
	 *
	 * @param string $date Unix timestamp
	 * @return boolean True if date is in this week
	 */
	public static function isThisWeek($date)
	{
		return date('W Y', $date) == date('W Y', time());
	}

	/**
	 * Returns true if given date is in this month
	 *
	 * @param string $date Unix timestamp
	 * @return boolean True if date is in this month
	 */
	public static function isThisMonth($date)
	{
		return date('m Y', $date) == date('m Y', time());
	}

	/**
	 * Returns either a relative date or a formatted date depending
	 * on the difference between the current time and given datetime.
	 * $datetime should be in a <i>strtotime</i>-parsable format, like MySQL's datetime datatype.
	 *
	 * Options:
	 *  'format' => a fall back format if the relative time is longer than the duration specified by end
	 *  'end' =>  The end of relative time telling
	 *
	 * Relative dates look something like this:
	 * 	3 weeks, 4 days ago
	 * 	15 seconds ago
	 * Formatted dates look like this:
	 * 	on 02/18/2004
	 *
	 * The returned string includes 'ago' or 'on' and assumes you'll properly add a word
	 * like 'Posted ' before the function output.
	 *
	 * @param string $dateString Datetime string or unix timestamp
	 * @param array $options Default format if timestamp is used in $dateString
	 * @return string Relative time string.
	 */
	public static function timeAgoInWords($dateString, $options = array())
	{
		$now = time();

		$inSeconds = self::getUnix($dateString);

		$backwards = ($inSeconds > $now);

		$format = 'j/n/y';
		$end = '+1 month';

		if (is_array($options)) {
			if (isset($options['format'])) {
				$format = $options['format'];
				unset($options['format']);
			}
			if (isset($options['end'])) {
				$end = $options['end'];
				unset($options['end']);
			}
		} else {
			$format = $options;
		}

		if ($backwards) {
			$futureTime = $inSeconds;
			$pastTime = $now;
		} else {
			$futureTime = $now;
			$pastTime = $inSeconds;
		}
		$diff = $futureTime - $pastTime;

		// If more than a week, then take into account the length of months
		if ($diff >= 604800) {
			$current = array();
			$date = array();

			list($future['H'], $future['i'], $future['s'], $future['d'], $future['m'], $future['Y']) = explode('/', date('H/i/s/d/m/Y', $futureTime));

			list($past['H'], $past['i'], $past['s'], $past['d'], $past['m'], $past['Y']) = explode('/', date('H/i/s/d/m/Y', $pastTime));
			$years = $months = $weeks = $days = $hours = $minutes = $seconds = 0;

			if ($future['Y'] == $past['Y'] && $future['m'] == $past['m']) {
				$months = 0;
				$years = 0;
			} else {
				if ($future['Y'] == $past['Y']) {
					$months = $future['m'] - $past['m'];
				} else {
					$years = $future['Y'] - $past['Y'];
					$months = $future['m'] + ((12 * $years) - $past['m']);

					if ($months >= 12) {
						$years = floor($months / 12);
						$months = $months - ($years * 12);
					}

					if ($future['m'] < $past['m'] && $future['Y'] - $past['Y'] == 1) {
						$years--;
					}
				}
			}

			if ($future['d'] >= $past['d']) {
				$days = $future['d'] - $past['d'];
			} else {
				$daysInPastMonth = date('t', $pastTime);
				$daysInFutureMonth = date('t', mktime(0, 0, 0, $future['m'] - 1, 1, $future['Y']));

				if (!$backwards) {
					$days = ($daysInPastMonth - $past['d']) + $future['d'];
				} else {
					$days = ($daysInFutureMonth - $past['d']) + $future['d'];
				}

				if ($future['m'] != $past['m']) {
					$months--;
				}
			}

			if ($months == 0 && $years >= 1 && $diff < ($years * 31536000)) {
				$months = 11;
				$years--;
			}

			if ($months >= 12) {
				$years = $years + 1;
				$months = $months - 12;
			}

			if ($days >= 7) {
				$weeks = floor($days / 7);
				$days = $days - ($weeks * 7);
			}
		} else {
			$years = $months = $weeks = 0;
			$days = floor($diff / 86400);

			$diff = $diff - ($days * 86400);

			$hours = floor($diff / 3600);
			$diff = $diff - ($hours * 3600);

			$minutes = floor($diff / 60);
			$diff = $diff - ($minutes * 60);
			$seconds = $diff;
		}
		$relativeDate = '';
		$diff = $futureTime - $pastTime;

		if ($diff > abs($now - strtotime($end))) {
			$relativeDate = sprintf('on %s', date($format, $inSeconds));
		} else {
			if ($years > 0) {
				// years and months and days
				$relativeDate .= ($relativeDate ? ', ' : '') . $years . ' ' . ($years == 1 ? 'year' : 'years');
				$relativeDate .= $months > 0 ? ($relativeDate ? ', ' : '') . $months . ' ' . ($months == 1 ? 'month' : 'months') : '';
				$relativeDate .= $weeks > 0 ? ($relativeDate ? ', ' : '') . $weeks . ' ' . ($weeks == 1 ? 'week' : 'weeks') : '';
				$relativeDate .= $days > 0 ? ($relativeDate ? ', ' : '') . $days . ' ' . ($days == 1 ? 'day' : 'days') : '';
			} elseif (abs($months) > 0) {
				// months, weeks and days
				$relativeDate .= ($relativeDate ? ', ' : '') . $months . ' ' . ($months == 1 ? 'month' : 'months');
				$relativeDate .= $weeks > 0 ? ($relativeDate ? ', ' : '') . $weeks . ' ' . ($weeks == 1 ? 'week' : 'weeks') : '';
				$relativeDate .= $days > 0 ? ($relativeDate ? ', ' : '') . $days . ' ' . ($days == 1 ? 'day' : 'days') : '';
			} elseif (abs($weeks) > 0) {
				// weeks and days
				$relativeDate .= ($relativeDate ? ', ' : '') . $weeks . ' ' . ($weeks == 1 ? 'week' : 'weeks');
				$relativeDate .= $days > 0 ? ($relativeDate ? ', ' : '') . $days . ' ' . ($days == 1 ? 'day' : 'days') : '';
			} elseif (abs($days) > 0) {
				// days and hours
				$relativeDate .= ($relativeDate ? ', ' : '') . $days . ' ' . ($days == 1 ? 'day' : 'days');
				$relativeDate .= $hours > 0 ? ($relativeDate ? ', ' : '') . $hours . ' ' . ($hours == 1 ? 'hour' : 'hours') : '';
			} elseif (abs($hours) > 0) {
				// hours and minutes
				$relativeDate .= ($relativeDate ? ', ' : '') . $hours . ' ' . ($hours == 1 ? 'hour' : 'hours');
				$relativeDate .= $minutes > 0 ? ($relativeDate ? ', ' : '') . $minutes . ' ' . ($minutes == 1 ? 'minute' : 'minutes') : '';
			} elseif (abs($minutes) > 0) {
				// minutes only
				$relativeDate .= ($relativeDate ? ', ' : '') . $minutes . ' ' . ($minutes == 1 ? 'minute' : 'minutes');
			} else {
				// seconds only
				$relativeDate .= ($relativeDate ? ', ' : '') . $seconds . ' ' . ($seconds == 1 ? 'second' : 'seconds');
			}

			if (!$backwards) {
				if ($relativeDate == '0 seconds')
					$relativeDate = 'Just now';
				else
					$relativeDate = sprintf('%s ago', $relativeDate);
			}
		}
		return $relativeDate;
	}

	/**
	 * formates a datetime string into a nice time format 8:30
	 * with the ability to add seconds
	 * @param type $datetime
	 * @param type $addSeconds
	 */
	public static function time($datetime, $addSeconds = 0)
	{
		$ts = self::datetimeToUnix($datetime);
		$ts += $addSeconds;
		return date('H:i', $ts);
	}

	/**
	 * convert a string mysql date to unix timestamp
	 * 
	 * @param string $date format: 2012-01-30
	 * @return int seconds unix timestamp 
	 */
	public static function dateToUnix($date)
	{
		if (!is_string($date) || $date == '')
			throw new CException('the $date parameter must be a string representation of a date, 2012-01-30');
		list($year, $month, $day) = explode('-', $date);
		return mktime(0, 0, 0, $month, $day, $year);
	}

	/**
	 * convert a unix timestamp into a mysql string datetime format
	 * @param int $unixTimestamp seconds since 1 ,1 1970 or null (if null will use the current time)
	 * @return string mydql datetime e.g.  2004-01-02 13:00:00 
	 */
	public static function unixToDatetime($unixTimestamp=null)
	{
		if ($unixTimestamp === null)
			$unixTimestamp = time();
		return date('Y-m-d H:i:s', $unixTimestamp);
	}

	/**
	 * convert a string mysql datetime to unix timestamp
	 * 
	 * @param string $datetime format: 2012-01-30 12:30:00
	 * @return int seconds unix timestamp 
	 */
	public static function datetimeToUnix($datetime)
	{
		if (!is_string($datetime) || $datetime == '')
			throw new CException('The $datetime parameter must be a string representation of a date, 2012-01-30 00:00:00 ' . $datetime);
		list($date, $time) = explode(' ', $datetime);
		list($year, $month, $day) = explode('-', $date);
		list($hours, $minutes, $seconds) = explode(':', $time);
		return mktime($hours, $minutes, $seconds, $month, $day, $year);
	}

	/**
	 * Transform time format string like "1:45" into the total number of minutes "105".
	 * @param int $hours
	 * @return string 
	 */
	public static function timeToMinutes($hours)
	{
		$minutes = 0;
		if (strpos($hours, ':') !== false) {
			// Split hours and minutes. 
			list($hours, $minutes) = explode(':', $hours);
		}
		return $hours * 60 + $minutes;
	}

	/**
	 * Converts total minutes to time format
	 * e.g. 105 minutes becomes 1:45
	 * @param int $minutes
	 * @return string 
	 */
	public static function minutesToTime($minutes)
	{
		$hours = (int) ($minutes / 60);
		$minutes -= $hours * 60;
		return sprintf("%d:%02.0f", $hours, $minutes);
	}

	/**
	 * converts total minutes into string "D:H:M"
	 * To get individual values for days, hours and minutes use the following code
	 * on the result of this function.
	 * <code>
	 * list($days, $hours, $mins) = explode(':', $result)
	 * </code>
	 * @param integer $minutes
	 * @param float $hoursPerDay the number of hours in the working day, defaults to 7
	 * @return string
	 */
	public static function minutesToDays($minutes, $hoursPerDay = 7)
	{
		$minsPerDay = $hoursPerDay * 60;
		$days = floor($minutes / $minsPerDay);

		$remainMinutes = $minutes % $minsPerDay;
		$hours = floor($remainMinutes / 60);
		$mins = floor($remainMinutes % 60);

		return $days . ":" . $hours . ":" . $mins;
	}

	/**
	 * Display $days $hours:$mins e.g.: 20 2:30
	 * @return string
	 */
	public static function minutesToDaysMini($minutes, $hoursPerDay = 7)
	{
		list($days, $hours, $mins) = explode(':', self::minutesToDays($minutes, $hoursPerDay));

		return "<strong>$days</strong> $hours:$mins";
	}

	/**
	 * formats the result of minutesToDays into a nice string like:
	 * 3 Days, 2 hours, 4 minutes
	 * 
	 * @see self::minutesToDays
	 * @param integer $minutes
	 * @param float $hoursPerDay the number of hours in the working day, defaults to 7.5
	 * @return string
	 */
	public static function minutesToNice($minutes, $hoursPerDay = 7)
	{
		list($days, $hours, $mins) = explode(':', self::minutesToDays($minutes, $hoursPerDay));
		$dLabal = ($days == 1) ? 'Day' : 'Days';
		$hLabal = ($hours == 1) ? 'Hour' : 'Hours';
		$mLabal = ($mins == 1) ? 'Minute' : 'Minutes';

		$ret = "";
		if ($days > 0)
			$ret .= "$days $dLabal";
		if ($hours > 0)
			$ret .= ($ret != '') ? ", $hours $hLabal" : "$hours $hLabal";
		if ($mins > 0)
			$ret .= ($ret != '') ? " and $mins $mLabal" : "$mins $mLabal";
		return $ret;
	}

	/**
	 * Converts minutes into the number of days as a decimal. e.g. 3.2 days
	 */
	public static function minutesToDaysDec($minutes, $hoursPerDay = 7)
	{
		return round($minutes / ($hoursPerDay * 60), 2);
	}

	/**
	 * formats the result of minutesToDays into a nice string like:
	 * 3 Days, 2:30 Hrs  |  4 Days, 3 Hrs
	 * 
	 * @see self::minutesToDays
	 * @param integer $minutes
	 * @param float $hoursPerDay the number of hours in the working day, defaults to 7.5
	 * @return string
	 */
	public static function minutesToNiceShort($minutes, $hoursPerDay = 7)
	{
		list($days, $hours, $mins) = explode(':', self::minutesToDays($minutes, $hoursPerDay));
		$dLabal = ($days == 1) ? 'day' : 'days';
		$ret = "";
		if ($days > 0)
			$ret .= "$days $dLabal";
		if ($hours > 0 || $mins > 0) {
			if ($days > 0)
				$ret .= ', ';
			$mins = ($mins > 0) ? ':' . str_pad($mins, 2, '0', STR_PAD_LEFT) : '';
			$ret.= "{$hours}{$mins} hrs";
		}

		return $ret;
	}

	/**
	 * get the number of working days between the $from and $to dates
	 * http://stackoverflow.com/questions/336127/calculate-business-days
	 * @param mixed $startDate string 2012-11-01 or int timestamp
	 * @param mixed $endDate date string 2012-11-01 or int timestamp
	 * @return The function returns the no. of business days between two dates and it skips the holidays
	 */
	public static function getWorkingDays($startDate, $endDate, $holidays=array())
	{
		// do strtotime calculations just once
		$endDate = self::getUnix($endDate);
		$startDate = self::getUnix($startDate);


		//The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
		//We add one to inlude both dates in the interval.
		$days = ($endDate - $startDate) / 86400 + 1;

		$no_full_weeks = floor($days / 7);
		$no_remaining_days = fmod($days, 7);

		//It will return 1 if it's Monday,.. ,7 for Sunday
		$the_first_day_of_week = date("N", $startDate);
		$the_last_day_of_week = date("N", $endDate);

		// The two can be equal in leap years when february has 29 days, the equal sign is added here
		// In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
		if ($the_first_day_of_week <= $the_last_day_of_week) {
			if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week)
				$no_remaining_days--;
			if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week)
				$no_remaining_days--;
		}
		else {
			// (edit by Tokes to fix an edge case where the start day was a Sunday
			// and the end day was NOT a Saturday)
			// the day of the week for start is later than the day of the week for end
			if ($the_first_day_of_week == 7) {
				// if the start date is a Sunday, then we definitely subtract 1 day
				$no_remaining_days--;

				if ($the_last_day_of_week == 6) {
					// if the end date is a Saturday, then we subtract another day
					$no_remaining_days--;
				}
			} else {
				// the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
				// so we skip an entire weekend and subtract 2 days
				$no_remaining_days -= 2;
			}
		}

		// The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
		// february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
		$workingDays = $no_full_weeks * 5;
		if ($no_remaining_days > 0) {
			$workingDays += $no_remaining_days;
		}

		//We subtract the holidays
		foreach ($holidays as $holiday) {
			$time_stamp = strtotime($holiday);
			//If the holiday doesn't fall in weekend
			if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
				$workingDays--;
		}

		return $workingDays;
	}

}