<?php if($apis) { ?>
<?php foreach($apis as $k=>$c) { ?>
<?php if(isset($c['apps'])) { ?>
<?php foreach($c['apps'] as $ka=>$a) { ?>
<?php if($appId == $a['id']) { ?>
<?php if(isset($a['apis'])) { ?>
<?php foreach($a['apis'] as $kapi=>$api) { ?>
<?php if($name == '' || strpos($api['name'], $name) !== false) { ?>
	<div class="crm-item crm-text-classic h30" style=" line-height: 20px;" type="item" mongo-id="<?php echo $api['id']; ?>">
		<div class="crm-button-float-left" style="font-size:18px; -webkit-transform: rotate(5deg);">
			<i class="icon-key"></i>
		</div>
		<div class="crm-button-float-left" style="margin-left:5px;">
			<?php echo $api['name']; ?>
		</div>
		<div class="crm-item-edit">
			<i class="icon-edit" style="font-size:18px; color:#333;"></i>
		</div>			
        <div class="crm-item-edit-json hide"><?php echo CHtml::encode(json_encode($api)); ?></div>
	</div>	
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>