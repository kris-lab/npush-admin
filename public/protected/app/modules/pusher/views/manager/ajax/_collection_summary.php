<?php
/*
 * Display summary information about collections
 */
?>

<div style="width:90%; margin:auto;">
	<div class="row-fluid">
			<div class="span6" style="text-align: center;">
				<h5>Activity collection</h5>
				<table class="table table-bordered table-striped" style="margin:auto;">
					<?php foreach($activity as $k=>$a) { ?>
						<?php if($k != 'ok' && $k != 'ns') { ?>
							<tr><td><?php echo $k; ?></td><td><?php echo is_array($a) ? print_r($a) : $a; ?></td></tr>
						<?php } ?>
					<?php } ?>
				</table>
			</div>
			<div class="span6" style="text-align: center;">
				<h5>Messages collection</h5>
				<table class="table table-bordered table-striped" style="margin:auto;">
					<?php foreach($message as $k=>$a) { ?>
						<?php if($k != 'ok' && $k != 'ns') { ?>
							<tr><td><?php echo $k; ?></td><td><?php echo is_array($a) ? print_r($a) : $a; ?></td></tr>
						<?php } ?>
					<?php } ?>
				</table>		
			</div>	
	</div>
</div>