<?php if($manager) { ?>
<?php if($clients = $manager->getClients($search, $sort)) { ?>
<?php if($clients->count()) { ?>
<?php foreach($clients as $k=>$a) { ?>
				<div class="crm-item crm-text-classic h30" style="line-height: 20px;" type="item" mongo-id="<?php echo $a['_id']; ?>">
					<div class="crm-button-float-left" style="font-size:25px; color:#08C; -webkit-transform: rotate(3deg);">
						<i class="icon-group"></i>
					</div>
					<div class="crm-button-float-left crm-text-fitted" style="margin-left:1px;">
						<?php echo $a['client_room']; ?>
					</div>
					<div class="crm-item-edit">
						<i class="icon-edit" style="font-size:18px; color:#333;" rel="tooltip" data-placement="top" data-original-title="Edit client"></i>
					</div>		
                    <div class="crm-item-edit-json hide"><?php echo CHtml::encode(json_encode($a)); ?></div>
				</div>		
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>