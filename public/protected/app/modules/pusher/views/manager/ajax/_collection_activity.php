<?php
/*
 * Display records from Activity Collection for API KEY
 */
?>

<?php if($data->count()) { ?>
    <table class="table table-bordered table-striped" style="font-size:12px; width:90%; margin: auto;">
        <thead>
            <th>User ID</th>
            <th>Last global activity</th>
            <th>Api/Channel/Rooms</th>
            <th>Last activity on path</th>
        </thead>
            <?php foreach($data as $k=>$d) { ?>		
                <tr>
                    <td rowspan="<?php echo count($d['rooms'])+1; ?>"><?php echo $d['user_id']; ?></td>
                    <td rowspan="<?php echo count($d['rooms'])+1; ?>"><?php echo date("H:i:s Y.m.d", $d['date']/1000); ?><br>TS:<?php echo $d['date_string']; ?></td>
                </tr>	
                <?php foreach($d['rooms'] as $kr=>$r) { ?>				
                    <tr>
						<td><?php echo str_replace('/'.$apiId, '', $kr); ?></td>						
                        <td><?php echo date("H:i:s Y.m.d", $r['date']/1000); ?><br>TS:<?php echo $r['date']; ?></td>
                    </tr>
                <?php } ?>				
        <?php } ?>
    </table>
<?php } else { ?>
    <div class="row-fluid">
        <div class="span-12" style="color:#666; text-align: center; padding-top: 30px; font-size:36px;">
            <h5 style=" font-size:22px;">No documents found</h5>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12" style="text-align:center;">
            <i class="icon-eye-close" style="font-size:110px; line-height: 130px; color:#333;"></i>
        </div>
    </div>
<?php } ?>
