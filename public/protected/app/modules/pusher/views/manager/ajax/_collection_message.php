<?php
/*
 * Display records from Activity Collection for API KEY
 */

function prettyPrint( $json )
{
    $result = '';
    $level = 0;
    $prev_char = '';
    $in_quotes = false;
    $ends_line_level = NULL;
    $json_length = strlen( $json );

    for( $i = 0; $i < $json_length; $i++ ) {
        $char = $json[$i];
        $new_line_level = NULL;
        $post = "";
        if( $ends_line_level !== NULL ) {
            $new_line_level = $ends_line_level;
            $ends_line_level = NULL;
        }
        if( $char === '"' && $prev_char != '\\' ) {
            $in_quotes = !$in_quotes;
        } else if( ! $in_quotes ) {
            switch( $char ) {
                case '}': case ']':
                    $level--;
                    $ends_line_level = NULL;
                    $new_line_level = $level;
                    break;

                case '{': case '[':
                    $level++;
                case ',':
                    $ends_line_level = $level;
                    break;

                case ':':
                    $post = " ";
                    break;

                case " ": case "\t": case "\n": case "\r":
                    $char = "";
                    $ends_line_level = $new_line_level;
                    $new_line_level = NULL;
                    break;
            }
        }
        if( $new_line_level !== NULL ) {
            $result .= "\n".str_repeat( "\t", $new_line_level );
        }
        $result .= $char.$post;
        $prev_char = $char;
    }

    return $result;
}

?>


<?php if($data->count()) { ?>
    <table class="table table-bordered table-striped" style="font-size:12px; width:90%; margin: auto;">
        <thead>
            <th>Timestamp / IP / Source</th>
            <th>Root / Channel / Room</th>
            <th>User</th>
            <th>Message</th>
			<th>Action</th>
        </thead>
        <tbody>
            <?php foreach($data as $d) { ?>
                <tr id="message-item-record-<?php echo $d['_id']; ?>">
                    <td>
						<?php echo date("H:i:s Y.m.d", $d['date_string']/1000); ?>
						<br>TS:<?php echo $d['date_string']; ?>
						<br>IP:<?php echo $d['client'] ?>
						<?php if(isset($d['source'])) { ?>							
							<br>S: <strong><?php echo $d['source'] == 1 ? 'POST':'SOCKET'; ?></strong>							
						<?php } ?>
					</td>
                    <td><?php echo str_replace('/'.$apiId, '', $d['room']); ?></td>
                    <td><?php echo $d['user']['id'].' : '.$d['user']['name']; ?></td>                    
                    <?php //echo nl2br(CHtml::encode(prettyPrint(json_encode($d['message'])))); ?>
                    <td><div class="json-item" id="message-json-item-<?php echo $d['_id']; ?>"><button class="btn btn-expand btn-small" onClick="JSONSyntaxHighlight('message-json-item-<?php echo $d['_id']; ?>');"><i class="icon-sitemap"></i></button>&nbsp;&nbsp;<span><?php echo CHtml::encode(str_replace(array('":"', '{"', '"}', '":', ',"'), array('" : "', '{ "', '" }', '" :', ', "'), json_encode($d['message']))); ?></span></div></td>
					<td>
						<p><a href="#" class="btn btn-small btn-info" onClick="ModulePusherDataCollectionMessageReceipt('<?php echo $d['_id']; ?>', this);">receipts</a></p>
						<p><a href="#" class="btn btn-small btn-danger" onClick="ModulePusherDataCollectionMessageDelete('<?php echo $d['_id']; ?>', this);">delete</a></p>
					</td>
                </tr>
				<tr class="hide message-item-<?php echo $d['_id']; ?>">
					<td colspan="5" class="message-item-body"></td>
				</tr>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <div class="row-fluid">
        <div class="span-12" style="color:#666; text-align: center; padding-top: 30px; font-size:36px;">
            <h5 style=" font-size:22px;">No documents found</h5>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12" style="text-align:center;">
            <i class="icon-eye-close" style="font-size:110px; line-height: 130px; color:#333;"></i>
        </div>
    </div>
<?php } ?>