<?php if($client) { ?>
<?php foreach($client as $k=>$c) { ?>
<?php if(isset($c['apps'])) { ?>
<?php foreach($c['apps'] as $ka=>$a) { ?>
<?php if($name == '' || strpos($a['name'], $name) !== false) { ?>
	<div class="crm-item crm-text-classic h30" style=" line-height: 20px;" type="item" mongo-id="<?php echo $a['id']; ?>">
		<div class="crm-button-float-left" style="font-size:22px; -webkit-transform: rotate(0deg);">
			<i class="icon-qrcode"></i>
		</div>
		<div class="crm-button-float-left" style="margin-left:5px;">
			<?php echo $a['name'] ; ?>
		</div>
		<div class="crm-item-edit">
			<i class="icon-edit" style="font-size:18px; color:#333;"></i>
		</div>
        <div class="crm-item-edit-json hide"><?php echo CHtml::encode(json_encode($a)); ?></div>
	</div>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>
<?php } ?>