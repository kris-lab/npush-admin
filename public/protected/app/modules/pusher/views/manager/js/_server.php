<script>
	
	// STATUS
	
	function ModuleServerNoConnection()
	{
		ModuleServerSelectedConnectionStatus = 0;
		
		$('#ModuleHeaderTitle').html(ModuleServerSelectedSettings.name 
					+ '<span class="label label-important" style="margin-left:10px;"><h5 style="line-height:1px;">NO SERVER CONNECTION</h5></span>'
					+ '&nbsp;&nbsp;<a href="#" class="btn btn-success btn-mini" onClick="ModuleServerGetList(\'update-selected\')"><i class="icon-refresh"></i></a>'			
				);
					
		ModuleManagerHideAppApiMenu();
	}
	
	// GET DATA

	function ModuleServerGetList(_action)
	{
		$('#ModuleServerModalSwitchServerList').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForServers/'); ?>', function(data) {
			
			if(data != '') {
				$('#ModuleServerModalSwitchServerList').html(data);
				ModuleManagerInitServersList();
			} else {
				$('#ModuleServerModalSwitchServerList').html('<div class="crm-item crm-item-empty">NO SERVERS</div>');
			}
			
			if(_action == 'update-selected') {
				
				ModuleServerSwitchTo(ModuleServerSelectedSettings.id);
			}
		});
	}	
	
	// MANAGEMENT
	
	function ModuleServerSwitchTo(_id)
	{
		try {

			ModuleServerSelectedSettings = JSON.parse($('#ModuleServerModalSwitchServerList').find('.server-item-id-' + _id).find('.server-item-settings').text());

			$('#ModuleHeaderTitle').html(ModuleServerSelectedSettings.name);
			ModuleManagerHideAppApiMenu();
			$('#ModulePreviewList').addPreview();
			ModulePusherGetClientListForServer();
			
			$('#ModuleServerCurrentMenu').show();
			
		} catch(e) {
			
			var servers = $('#ModuleServerModalSwitchServerList').children('.server-item').length;

			if(servers) {
				$('#ModuleServerModalSwitchServer').modal('show');
			} else {
				if(_id < 1) {
					$('#ModuleServerModalNew').modal('show');
				}
			}
		}		
	}
	
	// SETTINGS
	
	function ModuleServerSetDefault()
	{
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/setServerDefault/serverId'); ?>/' + ModuleServerDefaultId, function(data) {
			
			if(data != '') {
			
			}
		});
	}	
    
    // DELETE
    
    function ModuleServerEmptyMongoDB()
    {
		if(ModuleServerSelectedSettings.id < 1) return;
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/emptyMongoDatabase/serverId'); ?>/' + ModuleServerSelectedSettings.id, function(data) {
			
			if(data != '') {
                
                ModuleManagerHideAppApiMenu();
                $('#ModulePreviewList').addPreview();
                $('#ModuleServerModalServerMongoEmpty').modal('hide');
                ModulePusherGetClientListForServer();
			}
		});        
    }
	
    function ModuleServerDeleteConnection()
    {
		if(ModuleServerSelectedSettings.id < 1) return;
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/deleteServerConnection/serverId'); ?>/' + ModuleServerSelectedSettings.id, function(data) {
			
			if(data != '') {
                
				ModuleServerSelectedSettings.id = 0;
				ModuleManagerHideAppApiMenu();
				
				$('#ModuleContainerClientList').html();
				
				$('#ModuleServerCurrentMenu').hide();
                $('#ModulePreviewList').addPreview();
                $('#ModuleServerModalServerDelete').modal('hide');
                ModuleServerGetList();
			}
		});        
    }	
	
</script>