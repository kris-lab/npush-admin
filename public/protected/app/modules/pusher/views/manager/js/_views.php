
<script>
	
	var ModuleManagerPreviewExpand = 0;
	
	function ModuleUpdateViews() {
		
		$('#ModuleContainer').width($('#ModuleContainer').parent().width());
		$('#ModuleContainer').height($(window).height()-135);	
		$('#ModulePreviewList').height($('#ModuleContainer').height() - $('#ModulePreviewHeader').height());
        $('#ModulePreviewList').width($('#ModuleContainer').width()-$('#ModuleContainerApps').width()-$('#ModuleContainerApi').width()-$('#ModuleContainerClient').width());
		$('#ModulePreviewListTabs').height($('#ModulePreviewList').height()-$('#RBPagePropertiesTabs').height()-2);
		$('#ModulePreviewListTabs').find('.tab-pane').height($('#ModulePreviewListTabs').height()-0);
		
		$('#ModuleContainerClient').height($('#ModuleContainer').height());
		$('#ModuleContainerClientList').height($('#ModuleContainerClient').height() - 85);
		
		$('#ModulePreviewListTabData').height($('#ModulePreviewListTabs').height()-1);
		$('#ModulePreviewListTabGeneralTop').height($('#ModulePreviewListTabData').height() - $('#ModulePreviewListTabGeneralSender').height() - $('#ModulePreviewListTabGeneralChart__').height() - $('#ModulePreviewListTabGeneralSettings').height() - 63);
		$('#ModulePreviewListTabDataContainer').height($('#ModulePreviewListTabData').height() - $('#ModulePreviewListTabDataTop').height() - $('#ModulePreviewListTabDataBottom').height() - 13)
		$('#ModulePreviewListTabChatContainer').height($('#ModulePreviewListTabChat').height() - $('#ModulePreviewListTabChatTop').height() - $('#ModulePreviewListTabChatBottom').height() - 13)
		$('#ModulePreviewListTabStatContainer').height($('#ModulePreviewListTabStat').height() - $('#ModulePreviewListTabStatTop').height() - $('#ModulePreviewListTabStatBottom').height() - 13)
		
		$('#ModulePreviewListTabDataContainer').find('.tab-pane').height($('#ModulePreviewListTabDataContainer').height()-15);
		$('#ModulePreviewListTabs').find('.item-chart').width($('#ModulePreviewListTabs').width() - 35);
        
        ModulePreviewWidth = $('#ModulePreviewList').width();
	}	
	
	function ModuleManagerShowClientMenu()
	{
		setTimeout(function() {
			$('#ModuleContainerClient').animate({width:200,opacity:1},{
				duration: 100,
				step : function(now,fx) {
				
					$(this).show();
					$('#ModulePreviewList').width($('#ModuleContainer').width() - $('#ModuleContainerClient').width() - $('#ModuleContainerApps').width() - $('#ModuleContainerApi').width() - 5);					
				},
				complete: function() {
					
					ModuleManagerPreviewExpand = 0;
				}
			});
		},100);			
	}	
	
	function ModuleManagerShowAppMenu()
	{
		setTimeout(function() {
			$('#ModuleContainerApps').animate({width:200,opacity:1},{
				duration: 100,
				step : function(now,fx) {
				
					$(this).show();
					$('#ModulePreviewList').width($('#ModuleContainer').width() - $('#ModuleContainerApps').width() - $('#ModuleContainerApi').width() - $('#ModuleContainerClient').width() - 5);					
				},
				complete: function() {
					
                    ModuleManagerPreviewExpand = 0;
				}
			});
		},100);			
	}	
	
	function ModuleManagerShowApiMenu()
	{
		setTimeout(function() {
			$('#ModuleContainerApi').animate({width:200,opacity:1},{
				duration: 100,
				step : function(now,fx) {
				
					$(this).show();
					$('#ModulePreviewList').width($('#ModuleContainer').width() - $('#ModuleContainerApi').width() - $('#ModuleContainerApps').width() - $('#ModuleContainerClient').width() - 5);					
				},
				complete: function() {
					
                    ModuleManagerPreviewExpand = 0;
				}
			});
		},100);			
	}	
	
	function ModuleManagerHideApiMenu()
	{
        if(ModuleManagerSelectedAppId == '') return;
        
		setTimeout(function() {
			$('#ModuleContainerApi').animate({width:0,opacity:0},{
				duration: 100,
				step : function(now,fx) {
				
					$('#ModulePreviewList').width($('#ModuleContainer').width() - $('#ModuleContainerApi').width()-405);					
				},
				complete: function() {
					
					$(this).hide();
					//ModuleChartGenerator('ModulePreviewListTabGeneralChart', ["one", "two", "three", "four"], [{"name":"Example", "data":[1,2,3,4,5]}], 'Messages in time', 'CHART_PUSHER_GENERAL');
				}
			});
		},100);			
	}		
	
	function ModuleManagerHideAppApiMenu()
	{
        if(ModuleManagerSelectedAppId == '' && ModuleManagerSelectedApiId == '') return;
        
		if($('#ModuleContainerApi').width() > 0) {
			var _factor = 2;
		} else {
			var _factor = 1;
		}
		
		setTimeout(function() {
			$('#ModuleContainerApps, #ModuleContainerApi').animate({width:0,opacity:0},{
				duration: 100,
				step : function(now,fx) {
				
					$('#ModulePreviewList').width($('#ModuleContainer').width() - _factor*$('#ModuleContainerApps').width()-$('#ModuleContainerClient').width() - 5);					
				},
				complete: function() {
					
					$(this).hide();
				}
			});
		},100);			
	}	
	
	function ModuleManagerHideClientAppMenu()
	{
		setTimeout(function() {
			$('#ModuleContainerApps, #ModuleContainerClient').animate({width:0,opacity:0},{
				duration: 100,
				step : function(now,fx) {
				
					$('#ModulePreviewList').width($('#ModuleContainer').width() - 2*$('#ModuleContainerClient').width()-$('#ModuleContainerApi').width() - 5);					
				},
				complete: function() {
					
					$(this).hide();
					//ModuleChartGenerator('ModulePreviewListTabGeneralChart', ["one", "two", "three", "four"], [{"name":"Example", "data":[1,2,3,4,5]}], 'Messages in time', 'CHART_PUSHER_GENERAL');
				}
			});
		},100);			
	}

	function ModuleManagerHideClientApiMenu()
	{
		setTimeout(function() {
			$('#ModuleContainerApi, #ModuleContainerClient').animate({width:0,opacity:0},{
				duration: 100,
				step : function(now,fx) {
				
					$('#ModulePreviewList').width($('#ModuleContainer').width() - 2*$('#ModuleContainerClient').width()-$('#ModuleContainerApp').width() - 5);					
				},
				complete: function() {
					
					$(this).hide();
					//ModuleChartGenerator('ModulePreviewListTabGeneralChart', ["one", "two", "three", "four"], [{"name":"Example", "data":[1,2,3,4,5]}], 'Messages in time', 'CHART_PUSHER_GENERAL');
				}
			});
		},100);			
	}
	
	function ModuleManagerExpandPreview()
	{
		if(ModuleManagerPreviewExpand == 0) {
			
			var _width = 0;
			var _opacity = 0;
			ModuleManagerPreviewExpand = 1;
		} else {
			
			var _width = 200;
			var _opacity = 1;
			ModuleManagerPreviewExpand = 0;
		}
		
		setTimeout(function() {
            
            var _expand = '#ModuleContainerClient';
            
            if(ModuleManagerPreviewExpand == 0) {
                if(ModuleManagerSelectedClientId != '')
                    _expand = _expand + ', #ModuleContainerApps';

                if(ModuleManagerSelectedAppId != '')
                    _expand = _expand + ', #ModuleContainerApi';
            } else {
                _expand = _expand + ', #ModuleContainerApps, #ModuleContainerApi';
            }
            
			$(_expand).animate({width:_width,opacity:_opacity},{
				duration: 100,
				step : function(now,fx) {
				
					if(ModuleManagerPreviewExpand == 0) {
						$(this).show();
					}				
					$('#ModulePreviewList').width($('#ModuleContainer').width() - $('#ModuleContainerApi').width() - $('#ModuleContainerApps').width() - $('#ModuleContainerClient').width()-5);					
				},
				complete: function() {
					
					if(ModuleManagerPreviewExpand == 1) {
						$(this).hide();
					}
                    ModuleUpdateViews();
                    
                    if(ModuleManagerSelectedApiId != '')
                        ModuleChartInit();					
				}
			});
		},100);			
	}	
	
</script>