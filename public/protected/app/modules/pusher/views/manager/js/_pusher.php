<script>
	// GET DATA
	function ModulePusherGetClientListForServer()
	{
		if(ModuleServerSelectedSettings.id < 1) return;
		
		ModuleManagerShowClientMenu();
		$('#ModuleContainerClientList').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForClients/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/name/' + escape($('#ModuleContainerClientListInputSearch').val()).escapeSpecialChars(), function(data) {
			if(respondServer(data, 'respondStatus') == 'NO_DB_CONNECTION') {
				$('#ModuleContainerClientList').html('<div class="crm-item crm-item-empty">Cannot connect to DB</div>');
				ModuleServerNoConnection();				
			} else {
				if(data != '') {
					$('#ModuleContainerClientList').html(data);
					ModuleManagerInitClientList();
				} else {
					$('#ModuleContainerClientList').html('<div class="crm-item crm-item-empty">No clients in database</div>');
				}
				ModuleServerSelectedConnectionStatus = 1;
			}
		});
	}
	
	function ModulePusherGetAppsListForClient()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == '') return;
		
		$('#ModuleContainerAppsList').addSpinner();
		$('#ModulePreviewList').addPreview();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForClientApps/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/name/' + escape($('#ModuleContainerAppsListInputSearch').val()).escapeSpecialChars(), function(data) {
			if(data != '') {
				$('#ModuleContainerAppsList').html(data);
				ModuleManagerInitAppsList();
			} else {
				$('#ModuleContainerAppsList').html('<div class="crm-item crm-item-empty">No apps in database</div>');
			}
		});		
	}
	
	function ModulePusherGetApisListForApp()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == '') return;
		
		$('#ModuleContainerApisList').addSpinner();
		$('#ModulePreviewList').addPreview();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForClientAppAPIs/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/name/' + escape($('#ModuleContainerApisListInputSearch').val()).escapeSpecialChars(), function(data) {
			if(data != '') {
				$('#ModuleContainerApisList').html(data);
				ModuleManagerInitApisList();
			} else {
				$('#ModuleContainerApisList').html('<div class="crm-item crm-item-empty">No API keys in database</div>');
			}
		});	
	}
	
	function ModulePusherGetApiPreview()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$('#ModulePreviewList').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForClientAPIKey/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId, function(data) {
			if(data != '') {
				$('#ModulePreviewList').empty().html(data);
                ModuleUpdateViews();				
				ModuleManagerInitApiPreview();				
			} else {
				$('#ModulePreviewList').html('<div class="crm-item crm-item-empty">No apps in database</div>');
			}
		});
	}	
    
    function ModulePusherGetServerPreview()
    {
		if(ModuleServerSelectedSettings.id < 1) return;
        
        $('#ModulePreviewList').empty().html('<iframe id="ModulePreviewServerContainer" scrolling="no" style="width:99%;height:96%;border:0px solid black;margin-top:1px;padding:0px" frameborder="0"></iframe>');

		$('#ModuleContainerApi, #ModuleContainerClient, #ModuleContainerApps').hide().width(0);
		$('#ModulePreviewList').width($('#ModuleContainer').width());
		
		setTimeout(function() {
			var ifrm = document.getElementById('ModulePreviewServerContainer');
			ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
			ifrm.document.write('<div style="font-family:arial,tahoma;padding:50px;margin:auto;width:100%;text-align:center;height:100%;position:absolute;vertical-align:middle;">Loading...</div>');
			$('#ModulePreviewServerContainer').attr('src', '<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForServer/serverId/'); ?>/' + ModuleServerSelectedSettings.id);
		}, 10);       
    }
	
	// MONGO DATE VIEWER
	
	function ModulePusherDataCollectionMessages()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$('#ModulePreviewListTabDataContainerMessages').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForCollectionMessage/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId + '/name/' + escape($('#ModulePreviewListTabDataInputSearch').val()).escapeSpecialChars()+ '/limit/' + ModuleDataManageDocumentOnPage, function(data) {
			if(data != '') {
				$('#ModulePreviewListTabDataContainerMessages').empty().html(data);
			} else {
				$('#ModulePreviewList').html('<div class="crm-item crm-item-empty">No records in database</div>');
			}
		});
	}
	
	function ModulePusherDataCollectionMessageReceipt(_msg, owner)
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$item = $('#ModulePreviewListTabData').find('.message-item-'+_msg);
		$item.show().find('.message-item-body').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForCollectionMessageReceipt/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId + '/msgId/' + _msg, function(data) {
			$item.find('.message-item-body').html(data);
		});
	}	
    
	function ModulePusherDataCollectionMessageDelete(_msg, owner)
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$(owner).html('<i class="icon-time"></i>');
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/deleteItemFromMessageCollection/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId + '/msgId/' + _msg, function(data) {
			$('#message-item-record-'+_msg).fadeOut('slow', function() {
                $(this).remove();
            });
		});
	}    
	
	function ModulePusherDataCollectionActivity()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$('#ModulePreviewListTabDataContainerActivity').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForCollectionActivity/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId + '/name/' + escape($('#ModulePreviewListTabDataInputSearch').val()).escapeSpecialChars()+ '/limit/' + ModuleDataManageDocumentOnPage, function(data) {
			if(data != '') {
				$('#ModulePreviewListTabDataContainerActivity').empty().html(data);
			} else {
				$('#ModulePreviewList').html('<div class="crm-item crm-item-empty">No records in database</div>');
			}
		});
	}	
	
	function ModulePusherDataCollectionSummary()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$('#ModulePreviewListTabDataContainerSummary').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/getPreviewForCollectionSummary/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId, function(data) {
			if(data != '') {
				$('#ModulePreviewListTabDataContainerSummary').empty().html(data);
			} else {
				$('#ModulePreviewList').html('<div class="crm-item crm-item-empty">No records in database</div>');
			}
		});
	}		
	
	function ModulePusherDataCollectionRefreshView()
	{
		if(ModuleDataManageCollection == 'message') {
			ModulePusherDataCollectionMessages();
			$('#ModulePreviewListTabDataBottom').find('.row-fluid').fadeIn();
		} else if(ModuleDataManageCollection == 'activity') {
			ModulePusherDataCollectionActivity();
			$('#ModulePreviewListTabDataBottom').find('.row-fluid').fadeIn();
		} else {
			ModulePusherDataCollectionSummary();
			$('#ModulePreviewListTabDataBottom').find('.row-fluid').fadeOut();
		}
	}
	
	function ModulePusherDataCollectionSearch()
	{
		if(ModuleDataManageCollection == 'message') {
			ModulePusherDataCollectionMessages();
		} else if(ModuleDataManageCollection == 'activity') {
			ModulePusherDataCollectionActivity();
		}
	}	
	
	// DELETE DOCUMENTS
	
	function ModulePusherDataCollectionClear()
	{
		if(ModuleDataManageCollection == 'message') {
			ModulePusherDataCollectionMessageEmpty();
		} else if(ModuleDataManageCollection == 'activity') {
			ModulePusherDataCollectionActivityEmpty();
		}
	}
	
	function ModulePusherDataCollectionActivityEmpty()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$('#ModulePreviewListTabDataContainerActivity').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/emptyMongoCollectionActivityForAPI/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId, function(data) {
			
			ModulePusherDataCollectionSearch()
		});
	}
	
	function ModulePusherDataCollectionMessageEmpty()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$('#ModulePreviewListTabDataContainerMessages').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/emptyMongoCollectionMessageForAPI/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId, function(data) {
			
			ModulePusherDataCollectionSearch()
		});
	}
	
	function ModulePusherDeleteClient()
	{
		
	}
	
	function ModulePusherDeleteApp()
	{
		
	}
	
	function ModulePusherDeleteApi()
	{
		
	}
	
	// MANAGE APIs
	
	function ModulePusherAPISwitchToEnabled()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/aPIEnabled/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId, function(data) {

			ModulePusherGetAuthoristation();
			ModulePusherChatRefresh();
			$('#ModulePreviewListTabGeneralTop_nistatus').removeClass('label-important').addClass('label-success').html('API enabled');           
            
            $('#ModulePreviewListTabGeneralTop_msgperhour').fadeIn();
            $('#ModulePreviewListTabGeneralTop_msgperminute').fadeIn();
            $('#ModulePreviewListTabGeneralTop_activesocket').fadeIn();             
		});
	}
	
	function ModulePusherAPISwitchToDisabled()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;

		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/aPIDisabled/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId, function(data) {

			ModulePusherGetAuthoristation();
			ModulePusherChatRefresh();
			$('#ModulePreviewListTabGeneralTop_nistatus').removeClass('label-success').addClass('label-important').html('API disabled');
            
            $('#ModulePreviewListTabGeneralTop_msgperhour').fadeOut();
            $('#ModulePreviewListTabGeneralTop_msgperminute').fadeOut();
            $('#ModulePreviewListTabGeneralTop_activesocket').fadeOut();             
		});		
	}
	
	// PUSH CURL
	
	function ModulePusherGetAuthoristation()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$('#ModulePreviewListTabTestButtonCURL').removeClass('btn-success').addSpinner();
				
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/pusherGetStatus/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId, function(data) {

			$('#ModulePreviewListTabTestTextareaRespondCURL').prepend(data);
			$('#ModulePreviewListTabTestButtonCURL').addClass('btn-success').html('CURL now!');
						
			if(data.indexOf(': OK') > 0) {

				$('#ModulePreviewListTabGeneralTop_curlrequest').show().removeClass('label-important').addClass('label-success').html('Authorisated');
			} else if(data.indexOf(': AUTH_ERROR') > 0) {

				$('#ModulePreviewListTabGeneralTop_curlrequest').show().removeClass('label-success').addClass('label-important').html('Not authorisated');
			} else {
				
				$('#ModulePreviewListTabGeneralTop_curlrequest').show().removeClass('label-success').addClass('label-important').html('Push server connection failed');
			}
		});
	}
	
	function ModulePusherSendBroadCastMessage(_msg)
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
			
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/pusherBroadcastMessage/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/apiId/' + ModuleManagerSelectedApiId + '/message/' + escape(_msg).escapeSpecialChars(), function(data) {

		});
	}
	
	function ModulePusherSendBroadCastData(_data)
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
			
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/pusherBroadcastMessage/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/apiId/' + ModuleManagerSelectedApiId + '/message/' + escape(_data).escapeSpecialChars(), function(data) {

		});
	}	
	
	// PUSH PING
	
	function ModulePusherPingServer()
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		$('#ModulePreviewListTabTestButtonPingPush').removeClass('btn-success').addSpinner();
		
		$.get('<?php echo Yii::app()->createUrl('/pusher/manager/pusherServerPing/serverId/'); ?>/' + ModuleServerSelectedSettings.id, function(data) {
			
			$('#ModulePreviewListTabTestTextareaRespondPushPing').prepend(data);
			$('#ModulePreviewListTabTestTextareaRespondPushPing').scrollTop(0);
			$('#ModulePreviewListTabTestButtonPingPush').addClass('btn-success').html('Ping Server');
		});
	}
	
	// RESOURCES
	
	function ModulePusherDownloadLibraryWithSettings(_type)
	{
		if(ModuleServerSelectedSettings.id < 1
			|| ModuleManagerSelectedClientId == ''
			|| ModuleManagerSelectedAppId == ''
			|| ModuleManagerSelectedApiId == '') return;
		
		var ifrm = document.getElementById('ModuleHeaderIFrame');
		ifrm.src = '<?php echo Yii::app()->createUrl('/pusher/manager/pusherSetLibraryWithSettings/serverId/'); ?>/' + ModuleServerSelectedSettings.id + '/clientId/' + ModuleManagerSelectedClientId + '/appId/' + ModuleManagerSelectedAppId + '/apiId/' + ModuleManagerSelectedApiId + '/type/' + _type;		
	}
	
	// CHAT
	
	function ModulePusherChatSendRoomMessage()
	{
		window.frames['ModulePreviewListTabChatWindow' + ModuleChatUserSelected].sendMessage($('#ModulePreviewListTabChatInputMessage').val());
		$('#ModulePreviewListTabChatInputMessage').val('').focus();
	}
	
	function ModulePusherChatSendChannelMessage()
	{
		window.frames['ModulePreviewListTabChatWindow' + ModuleChatUserSelected].sendMessageToChannel($('#ModulePreviewListTabChatInputMessage').val());
		$('#ModulePreviewListTabChatInputMessage').val('').focus();
	}	
    
	function ModulePusherChatSendAppMessage()
	{
		window.frames['ModulePreviewListTabChatWindow' + ModuleChatUserSelected].sendMessageToApp($('#ModulePreviewListTabChatInputMessage').val());
		$('#ModulePreviewListTabChatInputMessage').val('').focus();
	}    
	
	function ModulePusherChatRefresh()
	{
		$('#ModulePreviewListTabChatContainer').find('iframe').each(function() {
			
			$(this).attr('src', $(this).attr('src'));
		});
	}
    
    // DASHBOARD
    
    function ModulePusherDashboardUpdate(_sockets_online)
    {
        $('#ModulePreviewListTabGeneralTop_activesocket').html(_sockets_online + ' sockets');
    }
    
    // experimental functions
	
	function ModulePusherDashboardStatusConnected() {
		
		$('#ModulePreviewListTabGeneralTop_curlrequest').removeClass('label-important').addClass('label-success').html('Connected');
		$('#ModulePreviewListTabGeneralTop_activesocket').show();
	}
	
	function ModulePusherDashboardStatusDisconnected() {
		
		$('#ModulePreviewListTabGeneralTop_curlrequest').removeClass('label-success').addClass('label-important').html('Disconnected');
		$('#ModulePreviewListTabGeneralTop_activesocket').hide();
	}
    
    function ModulePusherDashboardUpdateSpeedMeterScale(_direction) {
        
        // try if chart is alive
        if(CHART_PUSHER_SPEED_METER) {
            
            var max = CHART_PUSHER_SPEED_METER.yAxis[0].max;
            var point = CHART_PUSHER_SPEED_METER.series[0].points[0];
            
            point.update(0);                      
            
            if(_direction == '-') {
                
                CHART_PUSHER_SPEED_METER_YAXIS_MAX = CHART_PUSHER_SPEED_METER_YAXIS_MAX / 2;
                //CHART_PUSHER_SPEED_METER.yAxis[0].max = max/2;
                
            } else if(_direction == '+') {
                                
                CHART_PUSHER_SPEED_METER_YAXIS_MAX = CHART_PUSHER_SPEED_METER_YAXIS_MAX * 2;
            }
            
            CHART_PUSHER_SPEED_METER = ModuleChartGeneratorSpeed('ModulePreviewListTabGeneralTopChart', '', '', '', '');
        }        
    }
	
    function ModulePusherDashboardUpdateSpeedMeterScaleToValue(_value) {
        
        // try if chart is alive
        if(CHART_PUSHER_SPEED_METER) {
            
            var max = CHART_PUSHER_SPEED_METER.yAxis[0].max;
            var point = CHART_PUSHER_SPEED_METER.series[0].points[0];
            
            point.update(0);                      
                                
            CHART_PUSHER_SPEED_METER_YAXIS_MAX = _value;
            
            CHART_PUSHER_SPEED_METER = ModuleChartGeneratorSpeed('ModulePreviewListTabGeneralTopChart', '', '', '', '');
        }        
    }	
    
    function ModulePusherDashboardUpdateSpeedMeterValue(data) {
        
        // try if chart is alive
        if(CHART_PUSHER_SPEED_METER) {
            
            var point = CHART_PUSHER_SPEED_METER.series[0].points[0],
                newVal;
            
            if(data.mps < CHART_PUSHER_SPEED_METER_YAXIS_MAX) {
                newVal = data.mps;
            } else {
                newVal = CHART_PUSHER_SPEED_METER_YAXIS_MAX;
            }
            
            $('#ModulePreviewListTabGeneralTop_msgperhour').html(data.mph + ' msg&nbsp;/&nbsp;h');
            $('#ModulePreviewListTabGeneralTop_msgperminute').html(data.mpm + ' msg&nbsp;/&nbsp;m');
            
            point.update(newVal);
        }
        
        if(CHART_PUSHER_TRAFFIC_PEAK) {
            
            var series = CHART_PUSHER_TRAFFIC_PEAK.series[0];            
            var x = (new Date()).getTime()            
            series.addPoint([x, data.mps], true, true);
        }
    }
    // end of experimental    
</script>