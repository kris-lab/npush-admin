<?php
/**
 * Description of PusherManagerClient
 *
 * @author krzyztofstasiak
 */

class PusherManagerClient extends CFormModel {
	
	public $name;
    public $username;
	public $password;
	public $password_repeat;
	public $typesub;
	public $dscr;
	public $application_id;
 
	public function attributeLabels() {
        return array(
            'id' => 'Id',
			'name' => 'Name',
			'username' => 'Username',
			'password' => 'Password',
			'password_repeat' => 'Repeat password',
			'dscr' => 'Description',
			'application_id' => 'Identifier'
        );
    }	
	
    public function rules()
    {
        return array(
            array('name, username, password, password_repeat, application_id', 'required'),
            array('dscr, id', 'safe'),
        );
    }
}