<?php
/**
 * Description of PusherManagerServers
 *
 * @author krzyztofstasiak
 */

class PusherManagerServer extends NActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Client the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Gets the table name
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{pusher_manager_servers}}';
	}

	/**
	 * Gets the rules
	 * @return array of rules 
	 */
	public function rules()
	{
		return array(
			array('config_name, mongo_address, mongo_port, push_address, 
					push_port, push_version, mongo_dbname', 'required'),
			array('mongo_username, mongo_password
                    ,push_status_key, push_control_key', 'safe')
		);
	}
	
	/**
	 * Gets the relations
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}
	
	/**
	 * Contains an array of model attributes and their labels
	 * @return array Customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'config_name' => 'Name',
			'mongo_address' => 'Address',
			'mongo_port' => 'Port',
			'mongo_username' => 'Username',
			'mongo_password' => 'Password',
			'mongo_dbname' => 'DB Name',
			'push_address' => 'Address',
			'push_port' => 'Port',
			'push_version' => 'Version',
			'default' => 'Default',
            'push_control_key' => 'Admin Control Key',
            'push_status_key' => 'Admin Status Key'
		);
	}

	/**
	 * The table schema
	 * 
	 * @return array  
	 */
	public function schema()
	{
		return array
			(
			'columns' => array
				(
				'id' => "pk",
				'config_name' => 'string',
				'mongo_address' => 'string',
				'mongo_port' => 'string',
				'mongo_username' => 'string',
				'mongo_password' => 'string',
				'mongo_dbname' => 'string',
				'push_address' => 'string',
				'push_port' => 'string',
				'push_version' => 'string',
				'default' => 'bool DEFAULT 0',
                'push_control_key' => 'string',
                'push_status_key' => 'string'                
			),
			'keys' => array()
		);
	}

	/**
	 * Gets the behaviours which are attached to this model
	 * @return type 
	 */
	public function behaviors()
	{
		return array(
			'timestamp' => 'nii.components.behaviors.NTimestampable',
		);
	}
}